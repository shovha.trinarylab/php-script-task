#!/usr/bin/env php
<?php
require __DIR__.'/vendor/autoload.php';
use Symfony\Component\Console\Application;
use App\CreateTable;

$application = new Application();

$application->add(new CreateTable());


$application->run();
