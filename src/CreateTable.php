<?php

namespace App;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use PDO;

class CreateTable extends Command
{
    protected static $defaultName = 'create_table';
    private $usersTableName = 'users';
    private $databaseName = 'userdb';

    public function __construct()
    {
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Creates table.')
        ->setHelp('This command will create user table')
        ->addOption('username', 'u',  InputOption::VALUE_REQUIRED, 'username')
        ->addOption('password', 'p',  InputOption::VALUE_REQUIRED, 'password')
        ->addOption('host', null,  InputOption::VALUE_REQUIRED, 'host');


    }

    private function createUserTable($username, $password, $host)
    {
        $tableName = $this->usersTableName;
        $dbName = $this->databaseName;

        $dbCreateSql = "CREATE DATABASE IF NOT EXISTS `$dbName`";
        $userTableCreationSql = "CREATE TABLE $tableName (
            name VARCHAR(60) NOT NULL,
            surname VARCHAR(20) NOT NULL,
            email VARCHAR(40) PRIMARY KEY )";

        $conn = new PDO("mysql:host=$host;", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $conn->exec($dbCreateSql);
        $conn->query("use $dbName");
        $conn->exec($userTableCreationSql);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dbUsername = $input->getOption('username');
        $dbPassword = $input->getOption('password');
        $dbHost = $input->getOption('host');

        try {
            $this->createUserTable($dbUsername, $dbPassword, $dbHost);
            $output->writeln('connection success');
            }
        catch(PDOException $e)
            {
                $output->writeln('connection failed'. $e->getMessage());
            }
    }
}